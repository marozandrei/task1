package main

import (
	"bytes"
	"flag"
	"fmt"
	"strings"
	"task/pkg/consumer"
	"task/pkg/message"
)

const (
	BROKER = "127.0.0.1:9092"
	GROUP  = "group"
	TOPICS = "task1"
)

var (
	broker string
	topics string
	group  string
)

func init() {
	flag.StringVar(&group, "group", GROUP, "Kafka consumer group definition")
	flag.StringVar(&broker, "host", BROKER, "Kafka broker host:port")
	flag.StringVar(&topics, "topics", TOPICS, "Kafka topics to be consumed, as a comma separated list")
	flag.Parse()

	if broker == "" {
		flag.PrintDefaults()
	}
}

func main() {
	cValue := make(chan bytes.Buffer)
	defer close(cValue)

	c, err := consumer.NewConsumer(broker, group, strings.Split(topics, ","), cValue)
	if err != nil {
		panic(err)
	}
	defer c.Close()

	for {
		m := message.Decode(<-cValue)
		fmt.Printf("%v %v\n", m.Time, m.Value)
	}
}
