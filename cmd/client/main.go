package main

import (
	"bytes"
	"flag"
	"task/pkg/input"
	"task/pkg/message"
	"task/pkg/producer"
	"time"
)

const (
	BROKER = "127.0.0.1:9092"
	TOPIC  = "task1"
)

var (
	broker string
	topic  string
)

func init() {
	flag.StringVar(&broker, "host", BROKER, "Kafka broker host:port")
	flag.StringVar(&topic, "topic", TOPIC, "Kafka topic")
	flag.Parse()

	if broker == "" {
		flag.PrintDefaults()
	}
}

func main() {
	p, err := producer.NewProducer(broker)
	if err != nil {
		panic(err)
	}
	defer p.Close()

	cInput := make(chan string)
	defer close(cInput)
	cValue := make(chan bytes.Buffer)
	defer close(cValue)

	go input.Read(cInput)
	go createMessage(cInput, cValue)

	for {
		err := p.Send(<-cValue, topic)
		if err != nil {
			panic(nil)
		}
	}
}

// createMessage creates message, encodes it and sends to channel.
func createMessage(upstream chan string, downstream chan bytes.Buffer) {
	for {
		m := message.Message{
			Time:  int(time.Now().Unix()),
			Value: <-upstream,
		}

		downstream <- m.Encode()
	}
}
