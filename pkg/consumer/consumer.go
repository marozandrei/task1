// Package consumer implements consumer functionality for kafka.
package consumer

import (
	"bytes"
	"context"
	"github.com/Shopify/sarama"
)

// ConsumerGroupHandler interface.
type ConsumerGroupHandler interface {
	sarama.ConsumerGroupHandler
	WaitReady()
	Reset()
}

// Consumer class.
type Consumer struct {
	consumerGroup sarama.ConsumerGroup
}

// Close shuts down the consumer.
func (consumer *Consumer) Close() error {
	if consumer != nil {
		return consumer.consumerGroup.Close()
	}

	return nil
}

// NewConsumer creates a new SyncConsumerGroup.
func NewConsumer(broker, group string, topics []string, downstream chan bytes.Buffer) (*Consumer, error) {
	cfg := sarama.NewConfig()
	cfg.Version = sarama.V0_10_2_0
	cfg.Consumer.Offsets.Initial = sarama.OffsetOldest
	client, err := sarama.NewConsumerGroup([]string{broker}, group, cfg)
	if err != nil {
		return nil, err
	}

	consumer := Consumer{
		consumerGroup: client,
	}

	handler := NewSyncConsumerGroupHandler(func(data []byte) error {
		downstream <- *bytes.NewBuffer(data)
		return nil
	})

	ctx := context.Background()
	go func() {
		err := consumer.consumerGroup.Consume(ctx, topics, handler)
		if err != nil {
			panic(err)
		}
		if ctx.Err() != nil {
			panic(err)
		}
		handler.Reset()
	}()

	return &consumer, err
}
