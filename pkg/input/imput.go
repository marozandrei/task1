// Package for working with IO.
package input

import (
	"bufio"
	"os"
	"strings"
)

// Read data from IO by lines.
func Read(downstream chan string) {
	for {
		reader := bufio.NewReader(os.Stdin)

		text, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}

		downstream <- strings.Trim(text, "\n")
	}
}
