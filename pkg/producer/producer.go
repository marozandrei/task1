// Package producer implements producer functionality for kafka.
package producer

import (
	"bytes"
	"github.com/Shopify/sarama"
)

// Producer class.
type Producer struct {
	syncProducer sarama.SyncProducer
}

// Close shuts down the producer.
func (producer *Producer) Close() error {
	if producer != nil {
		return producer.syncProducer.Close()
	}

	return nil
}

// NewProducer creates a new SyncProducer.
func NewProducer(broker string) (*Producer, error) {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true

	producer, err := sarama.NewSyncProducer([]string{broker}, config)

	return &Producer{
		syncProducer: producer,
	}, err
}

// Send value to kafka.
func (producer *Producer) Send(value bytes.Buffer, topic string) error {
	_, _, err := producer.syncProducer.SendMessage(&sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.ByteEncoder(value.Bytes()),
	})

	return err
}
