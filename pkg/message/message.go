// Package message contains functionality to working with Message.
package message

import (
	"bytes"
	"encoding/gob"
)

// Message struct.
type Message struct {
	Time  int
	Value string
}

// Encode Message.
func (m Message) Encode() bytes.Buffer {
	var buff bytes.Buffer

	enc := gob.NewEncoder(&buff)
	err := enc.Encode(m)
	if err != nil {
		panic(err)
	}

	return buff
}

// Decode Message.
func Decode(buffer bytes.Buffer) Message {
	out := Message{}
	dec := gob.NewDecoder(&buffer)
	err := dec.Decode(&out)
	if err != nil {
		panic(err)
	}

	return out
}
