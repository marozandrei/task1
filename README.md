# Task 1
This project has two parts - server and clients.

## Usage

### Kafka
This project contains `docker-compose.yml` config file for run Kafka.

### Server
The server supports the following arguments:
```shell
-group string
    Kafka consumer group definition (default "group")
-host string
    Kafka broker host:port (default "127.0.0.1:9092")
-topics string
    Kafka topics to be consumed, as a comma separated list (default "task1")
```

The Server is started by the following command:
```shell
server.exe
```

### Client
The client supports the following arguments:
```shell
-host string
    Kafka broker host:port (default "127.0.0.1:9092")
-topic string
    Kafka topic (default "task1")
```

The Client is started by the following command:
```shell
client.exe
```
